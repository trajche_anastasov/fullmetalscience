---
vim: spell spelllang=en
layout: post
class: post-template
#cover: https://img.youtube.com/vi/H_kyYrbBY1I/0.jpg
cover: assets/images/H_kyYrbBY1I.jpg
title: "The Killer App: Bananas on the Blockchain?"
source: https://www.youtube.com/watch?v=H_kyYrbBY1I
tags: [Quotes]
author: aantonopoulos
---

Whatever you put "on the blockchain" will find a buyer. Or not? Andreas talks about the actual gems: sound money and governance. Here are a couple of hand-picked quotes from this talk.


h2. Quotes

p. _They don't believe you, right? They're like "we can do everything that Bitcoin does - except security - just as well with smart contracts"._ - https://www.youtube.com/watch?v=H_kyYrbBY1I#t=14m16s

p. _What they should actually be building is a decentralized exchange or wallet that actually works for real human beings._ - https://www.youtube.com/watch?v=H_kyYrbBY1I#t=29m31s

p. _Pollution isn't caused by energy consumption, Pollution is caused by some forms of energy production._ - https://www.youtube.com/watch?v=H_kyYrbBY1I#t=39m35s

p. _We have to do privacy on layer one. We cannot do privacy effectively on layer two. If you build a layer-two privacy solution with layer-one being wide open, that's like building SSL on top of a wide open internet._ - https://www.youtube.com/watch?v=H_kyYrbBY1I#t=50m50s
