---
vim: spell spelllang=en
layout: post
class: post-template
cover: assets/images/20200325_phoenix_runde.png
title: "phoenix runde: Die Coronakrise – Reichen die Maßnahmen? (17th of March 2020)"
source: https://www.phoenix.de/sendungen/gespraeche/phoenix-runde/die-coronakrise--reichen-die-massnahmen-a-1459505.html
tags: [Quotes]
author: kmolling
---

Prof. Karin Mölling, Virologist Max Planck Institute Berlin confirms that COVID-19 is a mild illness and that R0 is an unknown.

English translation followed by Spanish translation, followed by transcribed German original;


h2. Quotes

p. _So we have a mild disease. We have a sophisticated virus that can multiply in a very adventurous way so to say - and we have had others of this kind._
_Así que tenemos una enfermedad leve. Tenemos un sofisticado virus que puede multiplicarse de forma muy aventurera para así decirlo - y hemos tenido otros de este tipo._
_(Also wir haben eine milde Krankheit. Wir haben ein raffiniertes Virus was sich auf ganz abenteuerliche Weise sozusagen vermehren kann - und wir hatten auch schon andere dieser Art.)_ - https://www.phoenix.de/sendungen/gespraeche/phoenix-runde/die-coronakrise--reichen-die-massnahmen-a-1459505.html at 12m 15s

p. _Now we have people who [...] are infected and have no fever and have no symptoms. The exact rate of infection is not known._
_Ahora tenemos gente que [...] está infectada y no tiene fiebre ni síntomas. Se desconoce la tasa exacta de infección._
_(Jetzt haben wir Leute die [...] stecken an und haben gar kein Fieber und haben auch gar keine Symptome. Die genaue Ansteckungsrate da weiß man nicht.)_ - https://www.phoenix.de/sendungen/gespraeche/phoenix-runde/die-coronakrise--reichen-die-massnahmen-a-1459505.html at 30m 4s

p. _It is not such a bad epidemic [...] 3000 people die of normal age every day. 650,000 people die of influenza every year. You don't hear about it every year either._
_No es una epidemia tan grave [...] 3000 personas mueren de edad normal cada día. 650.000 personas mueren de gripe cada año. Tampoco se oye hablar de ello todos los años._
_(Es ist keine so schlimme Epidemie [...] Es sterben jeden Tag 3000 Leute an normalem Alter. [...] Es sterben im Jahr 650.000 an der Influenza. Davon hört man auch nicht jedes Jahr [...])_ - https://www.phoenix.de/sendungen/gespraeche/phoenix-runde/die-coronakrise--reichen-die-massnahmen-a-1459505.html at 40m 4s
