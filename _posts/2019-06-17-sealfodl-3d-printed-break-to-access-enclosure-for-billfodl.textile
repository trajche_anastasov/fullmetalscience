---
vim: spell spelllang=en
layout: post
class: post-template
cover: assets/images/sealfodl_for_billfodl.jpg
title: "Sealfodl - 3D-printed break-to-access enclosure for Billfodl"
canonical: https://www.reddit.com/r/Monero/comments/c1nn5i/sealfodl_3dprinted_breaktoaccess_enclosure_for/
tags: [Articles]
author: fullmetalScience
---

This is a step forward in the safety- & inheritance-concept I am currently developing for Monero.

Admittedly, branding this with it's own name might be a bit presumptuous, but heck ... I enjoy presenting things in a pleasant manner.

The "Sealfodl" is really just a tightly-fit enclosure that draws its value from the possibility of telling if someone has gained access to its content and thus potentially compromised your secrets (keys, mnemonic, ssss, ...).

A major part was finding a good and safe way to pause the print job and continue it from where it left off, allowing to hermetically seal the Billfodl in the process.

*Link:* "Sealfodl on Thingiverse":https://www.thingiverse.com/thing:3688625
